from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class AdminProjectsPage:

    button_add_project = (By.CSS_SELECTOR, "a[href='http://demo.testarena.pl/administration/add_project']")
    search_field = (By.CSS_SELECTOR, "#search")
    search_button = (By.CSS_SELECTOR, "#j_searchButton")

    def __init__(self, driver: WebDriver):
        self.driver = driver
        self.wait = WebDriverWait(driver, 10)

    def add_a_new_project(self):
        self.wait.until(EC.element_to_be_clickable(self.button_add_project))
        self.driver.find_element(*self.button_add_project).click()

    def search_project_by_name(self, project_name):
        self.wait.until(EC.visibility_of_element_located(self.search_field))
        self.driver.find_element(*self.search_field).send_keys(project_name)

    def click_search_button(self):
        self.wait.until(EC.element_to_be_clickable(self.search_button))
        self.driver.find_element(*self.search_button).click()

    def navigate_to_project(self, project_name):
        project_link_locator = (By.LINK_TEXT, project_name)
        self.wait.until(EC.element_to_be_clickable(project_link_locator))
        self.driver.find_element(*project_link_locator).click()
