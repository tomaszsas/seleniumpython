from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait


class AddNewAdminProjectPage:
    name_field = (By.ID, "name")
    prefix_field = (By.ID, "prefix")

    # For now in DOM the best way to find expanders is find color_expanders
    # and get the following elements:
    # Expander for new status is elements[0]
    # Expander for new status is elements[1]
    color_expanders = (By.CSS_SELECTOR, ".sp-replacer")

    # Fallowing locators find list of two elements
    # Color for new status is elements[0]
    # Color for in progress status is elements[1]
    green_color_in_color_palette_for_new_status = (By.CSS_SELECTOR, "span[data-color='rgb(0, 255, 0)']")
    yellow_color_in_color_palette_for_in_progress_status = (By.CSS_SELECTOR, "span[data-color='rgb(255, 255, 0)']")

    button_save = (By.CSS_SELECTOR, "input#save")

    def __init__(self, driver: WebDriver):
        self.driver = driver
        self.wait = WebDriverWait(driver, 10)

    def add_project_name(self, name):
        self.driver.find_element(*self.name_field).send_keys(name)

    def add_project_prefix(self, prefix):
        self.driver.find_element(*self.prefix_field).send_keys(prefix)

    def set_color_status_new(self):
        self.driver.find_elements(*self.color_expanders)[0].click()
        self.wait.until(lambda x: len(self.driver.find_elements(*self.green_color_in_color_palette_for_new_status)) == 2)
        self.driver.find_elements(*self.green_color_in_color_palette_for_new_status)[0].click()

    def set_color_status_in_progress(self):
        self.driver.find_elements(*self.color_expanders)[1].click()
        self.wait.until(lambda x: len(self.driver.find_elements(*self.yellow_color_in_color_palette_for_in_progress_status)) == 2)
        self.driver.find_elements(*self.yellow_color_in_color_palette_for_in_progress_status)[1].click()

    def save_project(self):
        self.driver.find_element(*self.button_save).click()
