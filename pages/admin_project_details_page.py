from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By


class AdminProjectDetailsPage:

    project_was_added_alert = (By.CSS_SELECTOR, "#j_info_box")
    page_endpoint = "http://demo.testarena.pl/administration/project_view"
    project_title = (By.CSS_SELECTOR, "#text.content_label_title")

    # For now there is no pretty locator for particular fields in project description.
    # In such situation I would ask developer to add more convenient attribute for these elements such as data-testid
    # For now I search particular fields in list of elements (in long term it is error-prone)
    # element[0] - prefix
    # element[1] - description
    # element[2] - status name
    # element [3] - color for new status
    # element [4] - color for in progress status
    # element [5] - date of project create
    project_fields = (By.CSS_SELECTOR, "#text.content_label")

    icon_color_status_new = (By.CSS_SELECTOR, "span[title=Nowe]")
    icon_color_status_in_progress = (By.CSS_SELECTOR, "span[title='W toku']")

    def __init__(self, driver: WebDriver):
        self.driver = driver

    def check_if_project_was_added_alert_is_present(self):
        return self.driver.find_element(*self.project_was_added_alert).is_displayed()

    def check_if_url_is_correct(self, url):
        return self.page_endpoint == url

    def get_project_name(self):
        return self.driver.find_element(*self.project_title).text

    def get_project_prefix(self):
        return self.driver.find_elements(*self.project_fields)[0].text

    def get_project_color_for_status_new(self):
        color_icon = self.driver.find_element(*self.icon_color_status_new)
        actual_color = color_icon.get_attribute("style").split(":")[1][1:-1]
        return actual_color

    def get_project_color_for_status_in_progress(self):
        color_icon = self.driver.find_element(*self.icon_color_status_in_progress)
        actual_color = color_icon.get_attribute("style").split(":")[1][1:-1]
        return actual_color
