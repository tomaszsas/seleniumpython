import pytest
from selenium.webdriver import Edge
from selenium.webdriver import Firefox
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.edge.service import Service as EdgeService
from selenium.webdriver.firefox.service import Service as FirefoxService
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.microsoft import EdgeChromiumDriverManager

from pages.login_page import LoginPage


@pytest.fixture
def chrome_browser():
    service = ChromeService(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)
    yield browser
    browser.quit()


@pytest.fixture
def edge_browser():
    service = EdgeService(EdgeChromiumDriverManager().install())
    browser = Edge(service=service)
    yield browser
    browser.quit()


@pytest.fixture
def firefox_driver():
    service = FirefoxService(GeckoDriverManager().install())
    browser = Firefox(service=service)
    yield browser
    browser.quit()


@pytest.fixture
def chrome_driver_test_arena(chrome_browser):
    login_page = LoginPage(chrome_browser)
    login_page.open_page().attempt_login('administrator@testarena.pl', 'sumXQQ72$L')
    yield chrome_browser
