import random
import string

from tests.conftest import chrome_browser, chrome_driver_test_arena
from pages.home_page import HomePage
from pages.messages_page import MessagesPage

administrator_email = 'administrator@testarena.pl'


def test_successful_login(chrome_driver_test_arena):
    home_page = HomePage(chrome_driver_test_arena)
    user_email = home_page.get_current_user_email()
    assert administrator_email == user_email


def test_add_message(chrome_driver_test_arena):
    my_text = generate_random_string(10)

    home_page = HomePage(chrome_driver_test_arena)
    home_page.click_mail_icon()

    messages_page = MessagesPage(chrome_driver_test_arena)
    messages_page.add_message(my_text)
    messages_page.verify_message_added(my_text)


def generate_random_string(length=10):
    characters = string.ascii_letters + string.digits
    random_string = ''.join(random.choice(characters) for _ in range(length))
    return random_string
