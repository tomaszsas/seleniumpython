# Proszę napisać Test, który:


# Wejdzie do sekcji Projects (lewe menu)
# Wyszuka nowo utworzony projekt po nazwie
# test_homework.py
# projekt na Gitlabie z rozwiązaniem
# POP, fixture, Wait (o ile trzeba)

import string

from components.admin_navigation_menu import AdminNavigationMenu
from pages.add_new_admin_project_page import AddNewAdminProjectPage
from pages.admin_project_details_page import AdminProjectDetailsPage
from pages.admin_projects_page import AdminProjectsPage
from pages.home_page import HomePage
from tests.conftest import chrome_driver_test_arena
import random


def test_add_a_new_project_correctly(chrome_driver_test_arena):
    expected_project_name = generate_random_string(10)
    expected_project_prefix = generate_random_string(5)
    green_color = "rgb(0, 255, 0)"
    yellow_color = "rgb(255, 255, 0)"

    home_page = HomePage(chrome_driver_test_arena)
    home_page.go_to_administration_page()

    admin_page = AdminProjectsPage(chrome_driver_test_arena)
    admin_page.add_a_new_project()

    add_new_project_page = AddNewAdminProjectPage(chrome_driver_test_arena)
    add_new_project_page.add_project_name(expected_project_name)
    add_new_project_page.add_project_prefix(expected_project_prefix)
    add_new_project_page.set_color_status_new()
    add_new_project_page.set_color_status_in_progress()
    add_new_project_page.save_project()

    admin_left_navigation_menu = AdminNavigationMenu(chrome_driver_test_arena)
    admin_left_navigation_menu.navigate_to_projects_page()

    admin_page.search_project_by_name(expected_project_name)
    admin_page.click_search_button()
    admin_page.navigate_to_project(expected_project_name)

    admin_project_details_page = AdminProjectDetailsPage(chrome_driver_test_arena)
    assert admin_project_details_page.get_project_name() == expected_project_name
    assert admin_project_details_page.get_project_prefix() == expected_project_prefix
    assert admin_project_details_page.get_project_color_for_status_new() == green_color
    assert admin_project_details_page.get_project_color_for_status_in_progress() == yellow_color


def generate_random_string(length):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))
