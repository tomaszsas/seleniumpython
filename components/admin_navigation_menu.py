from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By


class AdminNavigationMenu:

    project_page_button = (By.CSS_SELECTOR, "#left_header_menu a[href='http://demo.testarena.pl/administration/projects']")

    def __init__(self, driver: WebDriver):
        self.driver = driver

    def navigate_to_projects_page(self):
        self.driver.find_element(*self.project_page_button).click()
